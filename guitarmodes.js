/**
 * GuitarModes application by Sam Laidler, January 2017
 */
var originalImageWidth = 5312;
var originalImageHeight = 1373;
var imageResize = 0.1;
var imageWidth = originalImageWidth * imageResize;
var imageHeight = originalImageHeight * imageResize;
var modes = ["ionian", "dorian", "phrygian", "lydian", "mixolydian", "aeolian", "locrian"];

var img = new Image();
var stringPosition = [
  485, /* "E_LO":  */
  565, /* "A"   :  */
  641, /* "D"   :  */
  723, /* "G"   :  */
  803, /* "B"   :  */
  879 /* "E_HI":  */
];

var fretPosition = [
  4525, /* i */
  4117, /* ii */
  3753, /* iii */
  3433, /* iv */
  3133, /* v */
  2873, /* vi */
  2623, /* vii */
  2401, /* viii */
  2189, /* ix */
  2001, /* x */
  1819, /* xi */
  1647, /* xii */
  1495, /* xiii */
  1351, /* xiv */
  1207, /* xv */
  1081, /* xvi */
  965,  /* xvii */
  855,  /* xviii */
  751,  /* xix */
  655,  /* xx */
  571,  /* xxi */
  487   /* xxii */
];

var fretPositionLeft = [
	800, /* i */
	1200, /* ii */
	1557, /* iii */
	1881, /* iv */
	2177, /* v */
	2441, /* vi */
	2689, /* vii */
	2917, /* viii */
	3117, /* ix */
	3313, /* x */
	3501, /* xi */
	3661, /* xii */
	3821, /* xiii */
	3973, /* xiv */
	4105, /* xv */
	4233, /* xvi */
	4345, /* xvii */
	4453, /* xviii */
	4557, /* xix */
	4653, /* xx */
	4745, /* xxi */
	4833 /* xxii */
];

/*
 * Guitar string template
 * 
 * |---|---|---|---|---|
 * |---|---|---|---|---|
 * |---|---|---|---|---|
 * |---|---|---|---|---|
 * |---|---|---|---|---|
 * |---|---|---|---|---|
 *   |
 *   |
 *   |
 *   This is the starting fret regardless of where the first note starts. The
 *   starting fret of each mode is adjusted by the root offset. e.g. if
 *   rootOffset = -1, go back one. The starting fret is not necessarily
 *   pressed.
 */


/*
The chord I offset is denoted with a 'O' n the diagrams.

|-*-|-O-|---|-*-|---|
|-*-|-*-|---|-*-|---|
|-*-|---|-*-|-*-|---|
|-*-|---|-*-|-*-|---|
|---|-*-|---|-*-|---|
|-*-|-*-|---|-*-|---|

*/
var fingeringI = {
	"rootOffset" : -1,
	"chordIOffset" : 1,
	"fingering" : [ 1, 2, 4, 1, 2, 4, 1, 3, 4, 1, 3, 4, 2, 4, 1, 2, 4 ]
};

/*

|---|-*-|---|-*-|-*-|
|---|-*-|---|-*-|---|
|-*-|-O-|---|-*-|---|
|-*-|-*-|---|-*-|---|
|---|-*-|---|-*-|-*-|
|---|-*-|---|-*-|-*-|

*/
var fingeringII = {
	"rootOffset" : 1,
	"chordIOffset" : 6,
	"fingering" : [ 2, 4, 5, 2, 4, 1, 2, 4, 1, 2, 4, 2, 4, 5, 2, 4, 5 ]
};

/*

|-*-|-*-|---|-*-|---|
|-*-|---|-*-|-O-|---|
|-*-|---|-*-|-*-|---|
|-*-|---|-*-|---|---|
|-*-|-*-|---|-*-|---|
|-*-|-*-|---|-*-|---|

*/
var fingeringIII = {
	"rootOffset" : 4,
	"chordIOffset" : 5,
	"fingering" : [ 1, 2, 4, 1, 3, 4, 1, 3, 4, 1, 3, 1, 2, 4, 1, 2, 4 ]
};

/*

|---|-*-|---|-*-|---|
|-*-|-O-|---|-*-|---|
|-*-|-*-|---|-*-|---|
|-*-|---|-*-|-*-|---|
|---|-*-|---|-*-|-*-|
|---|-*-|---|-*-|---|

*/
var fingeringIV = {
	"rootOffset" : 6,
	"chordIOffset" : 3,
	"fingering" : [ 2, 4, 1, 2, 4, 1, 2, 4, 1, 3, 4, 2, 4, 5, 2, 4 ]
};

/*

|---|-*-|---|-*-|-O-|
|---|-*-|---|-*-|-*-|
|---|-*-|---|-*-|---|
|-*-|-*-|---|-*-|---|
|---|-*-|-*-|---|-*-|
|---|-*-|---|-*-|-*-|

*/
var fingeringV = {
	"rootOffset" : 8,
	"chordIOffset" : 2,
	"fingering" : [ 2, 4, 5, 2, 4, 5, 2, 4, 1, 2, 4, 2, 3, 5, 2, 4, 5 ]
};

var ionianScale = [0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 17, 19, 21, 23, 24, 26, 28, 29];
var setupModule = angular.module("setup", []);
setupModule.controller("SetupController",
	function($scope, $window) {
		disableSharp();
		disableFlat();
		disableNatural();
		
		$scope.showSetup = true;
		$scope.chords = [];
		$scope.addChord = function(addedChord) {
			$scope.chords.push(addedChord);
			$scope.chordsErrorText = "";
		}
		$scope.clearChords = function() {
			while ($scope.chords.length > 0) {
				$scope.chords.pop();
			}
		}
		$scope.checkKey = function(key) {
			if (key == "B" || key == "E") {
				disableSharp();
				if ($scope.accidental == "sharp") {
					$scope.accidental = "natural";
				}
			} else {
				enableSharp();
			}

			if (key == "C" || key == "F") {
				disableFlat();
				if ($scope.accidental == "flat") {
					$scope.accidental = "natural";
				}
			} else {
				enableFlat();
			}

			enableNatural();

			if ($scope.accidental == undefined) {
				$scope.accidental = "natural";
			}

			$scope.keyErrorText = "";
		}
		function disableSharp() {
			document.getElementById("radioSharp").disabled = true;
		}
		function disableFlat() {
			document.getElementById("radioFlat").disabled = true;
		}
		function disableNatural() {
			document.getElementById("radioNatural").disabled = true;
		}
		function enableSharp() {
			document.getElementById("radioSharp").disabled = false;
		}
		function enableFlat() {
			document.getElementById("radioFlat").disabled = false;
		}
		function enableNatural() {
			document.getElementById("radioNatural").disabled = false;
		}
		$scope.checkMode = function(mode) {
			$scope.modeErrorText = "";
		}
		$scope.checkTimeSig = function(timeSig) {
			$scope.timeSigErrorText = "";
		}
		$scope.checkBpm = function(bpm) {
			$scope.tempoErrorText = "";
		}
		$scope.removeItem = function(x) {
			$scope.errortext = "";
			$scope.chords.splice(x, 1);
		}

		$scope.practice = function() {
			var proceed = true;

			if (!$scope.key) {
				$scope.keyErrorText = "Please select";
				proceed = false;
			} else {
				$scope.keyErrorText = "";
			}

			if (!$scope.mode) {
				$scope.modeErrorText = "Please select";
				proceed = false;
			} else {
				$scope.modeErrorText = "";
			}

			if (!$scope.timeSig) {
				$scope.timeSigErrorText = "Please select";
				proceed = false;
			} else {
				$scope.timeSigErrorText = "";
			}

			if (!$scope.bpm) {
				$scope.tempoErrorText = "Please enter";
				proceed = false;
			} else if (isNaN($scope.bpm)) {
				$scope.tempoErrorText = "Please enter a number";
				proceed = false;
			} else {
				$scope.tempoErrorText = "";
			}

			if ($scope.chords.length == 0) {
				$scope.chordsErrorText = "Please select";
				proceed = false;
			} else {
				$scope.chordsErrorText = "";
			}

			if (proceed) {
				$scope.showSetup = false;
			}
		}

		img.addEventListener('load', function() {
			renderGuitarImages($scope);
		}, false);
		loadMidi();
		img.src = "fret_board.jpg";
		loadParameters($scope);

		$scope.setup = function() {
			$scope.showSetup = true;
			renderGuitarImages($scope);
		}

		$scope.stop = function() {
			window.clearInterval(playAndDrawHandle);
			document.getElementById("setup").disabled = false;
			document.getElementById("play").disabled = false;
			document.getElementById("flipH").disabled = false;
			document.getElementById("flipV").disabled = false;
			document.getElementById("stop").disabled = true;
			document.getElementById("loop").disabled = false;
		}

		$scope.flipH = function() {
			if (img.src.includes("fret_board.jpg")) {
				img.src = "fret_board_left.jpg";
			} else if (img.src.includes("fret_board_left.jpg")) {
				img.src = "fret_board.jpg";
			} else if (img.src.includes("fret_board_upturned.jpg")) {
				img.src = "fret_board_left_upturned.jpg";
			} else {
				img.src = "fret_board_upturned.jpg";
			}
		}

		$scope.flipV = function() {
			if (img.src.includes("fret_board.jpg")) {
				img.src = "fret_board_upturned.jpg";
			} else if (img.src.includes("fret_board_left.jpg")) {
				img.src = "fret_board_left_upturned.jpg";
			} else if (img.src.includes("fret_board_upturned.jpg")) {
				img.src = "fret_board.jpg";
			} else {
				img.src = "fret_board_left.jpg";
			}
		}
	}
);

function loadMidi() {
	MIDI.loadPlugin({
		soundfontUrl : "./soundfont/",
		instrument : "acoustic_grand_piano",
		onprogress : function(state, progress) {
			console.log(state, progress);
		},
		onsuccess : function() {
			console.log("Success");
		}
	});
}

function loadParameters($scope) {
	document.getElementById("stop").disabled = true;
	$scope.counter = 1;
	$scope.imageWidth = imageWidth;
	$scope.imageHeight = imageHeight;

	var sharpNotes = ["Fnatural", "Fsharp",
	                  "Gnatural", "Gsharp",
	                  "Anatural", "Asharp",
	                  "Bnatural",
	                  "Cnatural", "Csharp",
	                  "Dnatural", "Dsharp",
	                  "Enatural" ];
	var flatNotes = [ "Fnatural",
	                  "Gflat", "Gnatural",
	                  "Aflat", "Anatural",
	                  "Bflat", "Bnatural",
	                  "Cnatural",
	                  "Dflat", "Dnatural",
	                  "Eflat",
	                  "Fnatural" ];
	var chords = [ "I", "II", "III", "IV", "V", "VI", "VII" ];

	$scope.startPractice = function() {
		if ($scope.key == null || $scope.mode == null ||
			$scope.timeSig == null || $scope.bpm == null ||
			$scope.chords == null || $scope.chords.length == 0)
		{
			$scope.practiceErrorText = "No settings can be found. Go back to set up.";
			return;
		} else {
			$scope.practiceErrorText = "";
		}

		document.getElementById("setup").disabled = true;
		document.getElementById("play").disabled = true;
		document.getElementById("flipH").disabled = true;
		document.getElementById("flipV").disabled = true;
		document.getElementById("stop").disabled = false;
		document.getElementById("loop").disabled = true;

		if (($scope.accidental != "sharp") && ($scope.accidental != "flat")) {
			$scope.accidental = "natural";
		}
		
		var chosenKey = $scope.key + $scope.accidental;
		var chord;
		var startingFret;
		
		if ($scope.accidental == "sharp" || $scope.accidental == "natural") {
			startingFret = sharpNotes.indexOf(chosenKey);
		}
		else if ($scope.accidental == "flat") {
			startingFret = flatNotes.indexOf(chosenKey);
		}

		var chordsCopy = [];
		angular.copy($scope.chords, chordsCopy);
		firstCall = true;
		playAndDrawHandle = window.setInterval(
				function(){playAndDraw(startingFret, chordsCopy, $scope);},
				(60/$scope.bpm)*1000*$scope.timeSig); // assume time sig of 4/4

		/*
		 * Function fires on every bar
		 */
		function playAndDraw(startingFret, localChords, $scope) {
			var delay = 0; // play one note every quarter second
			var note = 53 + startingFret; // the MIDI note
			var velocity = 127; // how hard the note hits

			// first call: play one bar count in then exit
			if (firstCall) {
				chord = chords.indexOf(localChords[0]);
				var modeStartingPoint = ionianScale[modes.indexOf($scope.mode)];
				var triadI = ionianScale[chord + modes.indexOf($scope.mode)] - modeStartingPoint;
				for (i = 0; i < $scope.timeSig; i++) {
					MIDI.noteOn(0, note + triadI, velocity, delay);
					MIDI.noteOff(0, note + triadI, delay);
					delay += 60 / $scope.bpm;
				}
				firstCall = false;
				return;
			}

			chord = chords.indexOf(localChords.shift());

			var nextChord = null;
			if (localChords.length != 0) {
				nextChord = chords.indexOf(localChords[0]);
			}
			if ((localChords.length == 0) && ($scope.loop)) {
				nextChord = chords.indexOf($scope.chords[0]);
			}
			
			var modeStartingPoint = ionianScale[modes.indexOf($scope.mode)];
			var triadI = ionianScale[chord + modes.indexOf($scope.mode)] - modeStartingPoint;
			var triadII = ionianScale[chord + modes.indexOf($scope.mode) + 2] - modeStartingPoint;
			var triadIII = ionianScale[chord + modes.indexOf($scope.mode) + 4] - modeStartingPoint;

			// play the notes (triad)
			for (i = 0; i < $scope.timeSig; i++) {
				MIDI.noteOn(0, note + triadI, velocity, delay);
				MIDI.noteOff(0, note + triadI, delay);
				MIDI.noteOn(0, note + triadII, velocity, delay);
				MIDI.noteOff(0, note + triadII, delay);
				MIDI.noteOn(0, note + triadIII, velocity, delay);
				MIDI.noteOff(0, note + triadIII, delay);

				delay += 60 / $scope.bpm;
			}

			var canvasIndex = 1;
			drawDots(document.getElementById("canvas" + canvasIndex++),
					fingeringI, chord,
					startingFret + fingeringI.rootOffset,
					$scope, $scope.mode);
			drawDots(document.getElementById("canvas" + canvasIndex++),
					fingeringII, chord,
					startingFret + fingeringII.rootOffset,
					$scope, $scope.mode);
			drawDots(document.getElementById("canvas" + canvasIndex++),
					fingeringIII, chord,
					startingFret + fingeringIII.rootOffset, 
					$scope, $scope.mode);
			drawDots(document.getElementById("canvas" + canvasIndex++),
					fingeringIV, chord,
					startingFret + fingeringIV.rootOffset,
					$scope, $scope.mode);
			drawDots(document.getElementById("canvas" + canvasIndex++),
					fingeringV, chord,
					startingFret + fingeringV.rootOffset,
					$scope, $scope.mode);

			if (nextChord != null) {
				canvasIndex = 1;
				drawDots(document.getElementById("canvas" + canvasIndex++ + "next"),
					fingeringI, nextChord,
					startingFret + fingeringI.rootOffset,
					$scope, $scope.mode);
				drawDots(document.getElementById("canvas" + canvasIndex++ + "next"),
					fingeringII, nextChord,
					startingFret + fingeringII.rootOffset,
					$scope, $scope.mode);
				drawDots(document.getElementById("canvas" + canvasIndex++ + "next"),
					fingeringIII, nextChord,
					startingFret + fingeringIII.rootOffset, 
					$scope, $scope.mode);
				drawDots(document.getElementById("canvas" + canvasIndex++ + "next"),
					fingeringIV, nextChord,
					startingFret + fingeringIV.rootOffset,
					$scope, $scope.mode);
				drawDots(document.getElementById("canvas" + canvasIndex++ + "next"),
					fingeringV, nextChord,
					startingFret + fingeringV.rootOffset,
					$scope, $scope.mode);
			}
					
			if (localChords.length == 0) {
				if ($scope.loop) {
					angular.copy($scope.chords, localChords);
				} else {
					window.clearInterval(playAndDrawHandle);
					document.getElementById("setup").disabled = false;
					document.getElementById("play").disabled = false;
					document.getElementById("flipH").disabled = false;
					document.getElementById("flipV").disabled = false;
					document.getElementById("stop").disabled = true;
					document.getElementById("loop").disabled = false;
					renderNextGuitarImages($scope);
				}
			}
		}
	}
}

function renderGuitarImages($scope) {
    var canvas = document.getElementById("canvas1");
    var ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas2");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas3");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas4");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas5");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
	
	canvas = document.getElementById("canvas1next");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas2next");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas3next");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas4next");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas5next");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
}

function renderNextGuitarImages($scope) {
	var canvas = document.getElementById("canvas1next");
    var ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas2next");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas3next");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas4next");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
    
	canvas = document.getElementById("canvas5next");
    ctx = canvas.getContext("2d");
	ctx.drawImage(img,0,0, imageWidth, imageHeight);
}

function drawDots(canvas, fingering, chord, startingFret, $scope, mode) {
	var modeIndex = modes.indexOf(mode);
    var ctx = canvas.getContext("2d");
	var stringIndex = 0;
	var previousFingerIndex = 0;
	var repeatLow = false;
	var repeatHigh = false;
	var drawWhereItIs = false;
	var moveUp = false;
	console.log("**** chord **** = " + chord);
	var fretPosPtr = [];
	var fret;
	var stringPositionFinal;
	var indexI = fingering.chordIOffset + chord;
	var indexIII = indexI + 2;
	var indexV = indexI + 4;

	var root = 7;

	indexI += modeIndex;
	indexIII += modeIndex;
	indexV += modeIndex;

	// Can jump 2 octaves above so do it twice
	if (indexI >= root) {
		indexI -= root;
	}
	if (indexIII >= root) {
		indexIII -= root;
	}
	if (indexV >= root) {
		indexV -= root;
	}

	if (indexI >= root) {
		indexI -= root;
	}
	if (indexIII >= root) {
		indexIII -= root;
	}
	if (indexV >= root) {
		indexV -= root;
	}

	// get the highest value, we will need this later
	var highest = 0;
	for (fingerIndex = 0; fingerIndex < fingering.fingering.length; fingerIndex++) {
		if (fingering.fingering[fingerIndex] > highest) {
			highest = fingering.fingering[fingerIndex];
		}
	}

	// Having adjusted the fret for the mode, check if it's gone beyond the neck
	// If it has, move it up/down an octave
	if (startingFret + highest - ionianScale[modeIndex] - 1 > 21) {
		startingFret -= 12;
	} else if (startingFret - ionianScale[modeIndex] < 0) {
		// Don't bother checking the fingering, the leftmost will always be 1
		startingFret += 12;
	}

	// Left or right?
	if ((img.src.includes("fret_board.jpg")) || (img.src.includes("fret_board_upturned.jpg"))) {
		fretPosPtr = fretPosition;
	} else {
		fretPosPtr = fretPositionLeft;
	}

	var upturned = false;

	if ((img.src.includes("fret_board.jpg")) || (img.src.includes("fret_board_left.jpg"))) {
		stringIndex = 0;
	} else {
		stringIndex = 5;
		upturned = true;
	}

	/*
	 * Having moved the fingering up the neck as required to account for the
	 * mode check to see if we can repeat the fingering further up/down.
	 */
	if ( (startingFret - ionianScale[modeIndex] > 11) ) {
		repeatLow = true;
	}
	if (startingFret + highest - ionianScale[modeIndex] - 1 < 10) {
		repeatHigh = true;
	}

	for (fingerIndex = 0; fingerIndex < fingering.fingering.length; fingerIndex++) {
		// Move to the next string as required
		if (fingering.fingering[fingerIndex] < previousFingerIndex) {
			upturned ? stringIndex-- : stringIndex++;
		}

		console.log("fingerIndex = " + fingerIndex);
		console.log("root = " + root);
		console.log("indexI = " + indexI);
		console.log("indexIII = " + indexIII);
		console.log("indexV = " + indexV);
		
		if (fingerIndex % root == indexI) {
			ctx.fillStyle = "#FF0000";
		}
		else if (fingerIndex % root == indexIII || fingerIndex % root == indexV) {
			ctx.fillStyle = "#FFBB00";
		}
		else {
			ctx.fillStyle = "#0088FF";
		}
		
		console.log("ctx.fillStyle = " + ctx.fillStyle);
	
		// Calculate the next fret 
		fret = startingFret + fingering.fingering[fingerIndex] - ionianScale[modeIndex] - 1;
		
		// Bring it down/up slightly the closer you get to the body as the guitar neck is tilted
		var adjuster = (20 * (fingerIndex / fingering.fingering.length));
		if (upturned) {
			stringPositionFinal = stringPosition[stringIndex] - adjuster;
		}
		else {
			stringPositionFinal = stringPosition[stringIndex] + adjuster;
		}

	    ctx.beginPath();
		ctx.arc(fretPosPtr[fret] * imageResize, stringPositionFinal * imageResize,4,0,2*Math.PI);
		ctx.fill();
		
		if (repeatLow) {
		    ctx.beginPath();
			ctx.arc(fretPosPtr[fret - 12] * imageResize,
					stringPositionFinal * imageResize,4,0,2*Math.PI);
			ctx.fill();
		}
		if (repeatHigh) {
		    ctx.beginPath();
			ctx.arc(fretPosPtr[fret + 12] * imageResize,
					stringPositionFinal * imageResize,4,0,2*Math.PI);
			ctx.fill();
		}

		previousFingerIndex = fingering.fingering[fingerIndex];
	}
}
