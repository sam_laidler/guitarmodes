# My project's README

This project has a dependency on the following MIDI project:

https://github.com/mudcube/MIDI.js

The MIDI folder can be downloaded and run on Tomcat as shown in the following example:

C:\apache-tomcat-8.5.11\webapps\midi

Code for this project can then be run from the following subdirectory:

C:\apache-tomcat-8.5.11\webapps\midi\guitarmodes\

Start Tomcat and then navigate to:

http://localhost:2020/midi/guitarmodes/index.html

This will obviously depend on port configuration/folder setup.

On AWS:

Redirect 8080 to 80:

sudo /sbin/iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080
sudo /sbin/iptables -t nat -I PREROUTING -p tcp --dport 443 -j REDIRECT --to-port 8443
sudo service iptables save

Add proxyPort to:

<Connector port="8443" proxyPort="443" .../>
<Connector port="8080" proxyPort="80" .../>

Folder structure:

[ec2-user@ip-172-31-6-69 midi]$ pwd
/home/ec2-user/midi
[ec2-user@ip-172-31-6-69 midi]$ ls -l
total 8332
drwxr-xr-x 2 root root    4096 Mar  6 15:18 build
drwxr-xr-x 6 root root    4096 Mar 10 08:09 examples
-rw-r--r-- 1 root root 1553436 Apr  4 13:09 fret_board.jpg
-rw-r--r-- 1 root root 1535692 Apr 27 06:42 fret_board_left.jpg
-rw-r--r-- 1 root root 1534171 Jun 30 14:58 fret_board_left_upturned.jpg
-rw-r--r-- 1 root root 2257738 Jan 22 16:27 fret_board_original.jpg
-rw-r--r-- 1 root root 1546605 Jun 30 14:57 fret_board_upturned.jpg
drwxr-xr-x 4 root root    4096 Mar  6 15:18 generator
-rw-r--r-- 1 root root    1182 Jul 13  2015 Gruntfile.js
drwxr-xr-x 3 root root    4096 Jul 18 06:02 guitarmodes
-rw-r--r-- 1 root root     786 Mar 31 06:30 guitarmodes.css
-rw-r--r-- 1 root root   17877 Jul  6 07:13 guitarmodes.js
drwxr-xr-x 5 root root    4096 Mar  6 15:18 inc
-rw-r--r-- 1 root root    6692 Jul 14 06:18 index.html
drwxr-xr-x 4 root root    4096 Mar  6 15:18 js
drwxr-xr-x 2 root root    4096 Mar 10 16:07 lib
-rw-r--r-- 1 root root    1102 Jul 13  2015 LICENSE.txt
-rw-r--r-- 1 root root     523 Jul 13  2015 package.json
-rw-r--r-- 1 root root     537 Jun 14 06:49 README_GM.md
-rw-r--r-- 1 root root    8026 Jul 13  2015 README.md
drwxr-xr-x 4 root root    4096 Mar  6 15:18 soundfont
-rw-r--r-- 1 root root    1025 Jul 13  2015 TODO
[ec2-user@ip-172-31-6-69 midi]$
